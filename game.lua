local colors = require("modules.colors")
local settings = require("modules.settings")
local UI = require("modules.ui")
collectgarbage()

function setup()
    -- Main variables
    width = 480
    height = 272

    --Setting vars
    soundStatus = settings.get("sound")
    debugStatus = settings.get("debug")
    save = tonumber(settings.get("lastsave"))

    --Sound vars
    background = sound.load("resources/music/menu.mp3")
    sound.loop(background)
    if soundStatus then
        sound.play(background)
    end

    --Menubar vars
    topbar = UI.Menubar.new(8, "resources/sprites/menubar/menubar.png", colors.White(), colors.Asbestos(), "text", colors.Black())
    buttonEnabled = {l = false, r = false, up = false, down = false, left = false, right = false, cross = false, circle = false, triangle = false, square = false}

    --Misc vars
    starttime = os.clock()
    currentlegend = 1
    legend = {
        "X: Select    O: Back    △: Delete    □: Build",
        --"Left: Move to quadrant    Right: Move to quadrant",
        "START: Pause game",
        "L: Menu left    R: Menu right"
    }
    img = image.load("test.png")
    cursor = image.load("resources/sprites/cursor.png")
    cursorX = 20
    cursorY = 20

    function string.lines(string)
        local n=0
        for i in string:gmatch("\n") do n=n+1 end
        return n+1
    end
end

function screen.center(y, text, size)
    screen.print((width-screen.textwidth(text, (size == nil and 0.7) or size))/2, y, text, (size == nil and 0.7) or size)
end

function drawGrid()
    --Horizontal
    local i = 17
    repeat
        draw.line(0, i, 480, i, colors.Asbestos())
        i = i +20
    until (i > 264)

    --Vertical
    i = 0
    repeat
        draw.line(i, 17, i, 257, colors.Asbestos())
        i = i +20
    until (i > 480)
    draw.line(480, 17, 479, 257, colors.Asbestos())
end

function buttons.run()
    if buttons.released.l then
        local index = UI.Menubar.getIndex(topbar) -1
        if(index < 1) then
            index = UI.Menubar.maxItems(topbar)
        end
        UI.Menubar.change(topbar, index)
    end
    if buttons.released.r then
        local index = UI.Menubar.getIndex(topbar) +1
        if(index > UI.Menubar.maxItems(topbar)) then
            index = 1
        end
        UI.Menubar.change(topbar, index)
    end

    if buttons.analogx > 16 then
        cursorX = cursorX + 2
    elseif buttons.analogx > 32 then
        cursorX = cursorX + 4
    elseif buttons.analogx > 64 then
        cursorX = cursorX + 6
    end

    if buttons.analogx < -16 then
        cursorX = cursorX - 2
    elseif buttons.analogx < -32 then
        cursorX = cursorX - 4
    elseif buttons.analogx < -64 then
        cursorX = cursorX - 6
    end

    if buttons.analogy > 16 then
        cursorY = cursorY - 2
    elseif buttons.analogY > 32 then
        cursorY = cursorY - 4
    elseif buttons.analogY > 64 then
        cursorY = cursorY - 6
    end

    if buttons.analogy < -16 then
        cursorY = cursorY + 2
    elseif buttons.analogY < -32 then
        cursorY = cursorY + 4
    elseif buttons.analogY < -64 then
        cursorY = cursorY + 6
    end

end

-- Main loop
function loop()
    UI.Menubar.show(topbar)
    if os.clock()-starttime > 10 then
        starttime = os.clock()
        currentlegend = currentlegend +1
        if currentlegend == #legend+1 then
            currentlegend = 1
        end
    end
    image.blit(img, 0, 17)
    screen.center(height-15, legend[currentlegend], 0.6)
    drawGrid()

    image.blit(cursor, cursorX, cursorY)
end

function debug()
    -- Add here the code for the debug functions
    if debugStatus then
        function round(num, numDecimalPlaces)
            local mult = 10^(numDecimalPlaces or 0)
            return math.floor(num * mult + 0.5) / mult
        end
        screen.print(341, 0, "FPS: " .. screen.fps(), 0.5, colors.Black())
    end
end

-- START - Don't modify this block
setup()
loop()
while true do
    buttons.read()
    buttons.run()
    loop()
    debug()
    screen.flip()
    collectgarbage("collect")
end
-- END - Don't modify this block
