local path = "config.cfg"
if not files.exists(path) then
    local settings = "sound = true\ndebug = false\nautosave = true\nlastsave = 1"
    local file = io.open(path, "w")
    file:write(settings)
    file:close()
end

--Bypass menu
--dofile("menu.lua")
dofile("game.lua")
