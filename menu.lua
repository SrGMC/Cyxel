local colors = require("modules.colors")
local settings = require("modules.settings")
local UI = require("modules.ui")

function setup()
    --Global vars
    width = 480
    height = 272
    window = "main"

    --Menu vars
    index = nil
    count = 0
    enableCount = false
    main = UI.Menu.new("Cixel", {"Load game", "Create game", "Game settings", "Exit to XMB"}, colors.Red(), colors.Asbestos())
    saves = UI.Menu.new((((window == "load") and "Load") or "Create") .. " Game", {"Save 1", "Save 2", "Save 3", "Save 4"}, colors.Red(), colors.Asbestos())
    config = UI.Menu.new("Settings", {"Sound: " .. ((soundStatus and "enabled") or "muted"), "Autosave: " .. ((autosaveStatus and "enabled") or "disabled"), "Debug mode: " .. ((debugStatus and "enabled") or "disabled"), "Credits"}, colors.Red(), colors.Asbestos())
    currentmenu = main

    --Alert vars
    overwrite = UI.Alert.new("Warning", "This action will overwrite the save file\nAll your progress will be lost\nDo you wish to continue?", "Yes", "No", colors.Yellowish(), colors.White(), colors.Red(), colors.Yellowish(), colors.White(), 2)
    delete = UI.Alert.new("Warning", "This action will delete the save file\nDo you wish to continue?", "Yes", "No", colors.Yellowish(), colors.White(), colors.Red(), colors.Yellowish(), colors.White(), 2)
    notexists = UI.Alert.new("Error", "Save file does not exist", "Ok", "", colors.Red(), colors.White(), colors.Red(), colors.White(), colors.White(), 1)
    currentalert = overwrite
    showerror = false; showdelete = false

    --Button variables
    buttonEnabled = {l = false, r = false, up = false, down = false, left = false, right = false, cross = false, circle = false, triangle = false, square = false}

    --Sound vars
    background = sound.load("resources/music/menu.mp3")
    sound.loop(background)
    if settings.get("sound") then
        sound.play(background)
    end

    --Settings variables
    soundStatus = settings.get("sound")
    autosaveStatus = settings.get("autosave")
    debugStatus = settings.get("debug")
end

--Print text at y coordinates centered in the x coordinates
function screen.center(y, text, size)
    screen.print((width- screen.textwidth(text, (size == nil and 0.7) or size))/2, y, text, (size == nil and 0.7) or size)
end

--Checks if a savefile exists
function checksave(saveid)
    if files.exists("saves/save" .. saveid .. ".sav") then
        return true
    end
    return false
end

--Main loop
function loop()
    buttonEnabled = {l = false, r = false, up = false, down = false, left = false, right = false, cross = false, circle = false, triangle = false, square = false}
    if showerror then
        buttonEnabled.cross = true; buttonEnabled.circle = true
        UI.Alert.show(notexists)
        screen.center(height-20, "X: Select    O: Go back", 0.6)
    elseif showdelete then
        buttonEnabled.left = true; buttonEnabled.right = true; buttonEnabled.cross = true; buttonEnabled.circle = true
        UI.Alert.show(delete)
        screen.center(height-20, "X: Confirm    O: Go back", 0.6)
    elseif window == "main" then
        buttonEnabled.up = true; buttonEnabled.down = true; buttonEnabled.cross = true
        UI.Menu.show(main, index)
        screen.center(height-20, "X: Select item", 0.6)
    elseif window == "load" or window == "create" then
        buttonEnabled.up = true; buttonEnabled.down = true; buttonEnabled.cross = true; buttonEnabled.circle = true; buttonEnabled.triangle = true
        UI.Menu.setTitle(saves, (((window == "load") and "Load") or "Create") .. " Game")
        UI.Menu.show(saves, index)
        screen.center(height-20, "X: Select item    O: Go back    △: Delete save", 0.6)
    elseif window == "settings" then
        buttonEnabled.up = true; buttonEnabled.down = true; buttonEnabled.cross = true; buttonEnabled.circle = true
        UI.Menu.setMenulist(config, {"Sound: " .. ((soundStatus and "enabled") or "muted"), "Autosave: " .. ((autosaveStatus and "enabled") or "disabled"), "Debug mode: " .. ((debugStatus and "enabled") or "disabled"), "Credits"})
        UI.Menu.show(config, index)
        screen.center(height-20, "X: Change item    O: Go back", 0.6)
    elseif window == "overwrite" then
        buttonEnabled.left = true; buttonEnabled.right = true; buttonEnabled.cross = true; buttonEnabled.circle = true
        UI.Alert.show(overwrite)
        screen.center(height-20, "X: Confirm    O: Go back", 0.6)
    end
end

function buttons.run()
    if buttons.released.up and buttonEnabled.up then            --Move menu selection up
        local index = UI.Menu.getIndex(currentmenu) -1
        if(index < 1) then
            index = 4
        end
        UI.Menu.change(currentmenu, index)
    end
    if buttons.released.down and buttonEnabled.down then        --Move menu selection down
        local index = UI.Menu.getIndex(currentmenu) +1
        if(index > 4) then
            index = 1
        end
        UI.Menu.change(currentmenu, index)
    end
    if buttons.released.left and buttonEnabled.left then        --Move alert selection left
        local index = UI.Alert.getIndex(currentalert) - 1
        if(index < 1) then
            index = 1
        end
        UI.Alert.change(currentalert, index)
    end
    if buttons.released.right and buttonEnabled.right then      --Move alert selection right
        local index = UI.Alert.getIndex(currentalert) +1
        if(index > 2) then
            index = 2
        end
        UI.Alert.change(currentalert, index)
    end
    if buttons.released.cross and buttonEnabled.cross then      --Selection
        if showerror then                                       --If it is on error, close it
            showerror = false
        elseif showdelete then                                  --If it is on delete, delete file on yes, ignore on no.
            if UI.Alert.getIndex(delete) == 1 then
                settings.remove(settings.get("lastsave"))
            end
            showdelete = false                                      --Hide delete alert
            UI.Alert.change(delete, 2)                                 --Reset default selection
        elseif window == "overwrite" then                       --If it is on overwrite, overwrite and load on yes, hide and go back on no.
            if UI.Alert.getIndex(overwrite) == 1 then
                settings.save(settings.get("lastsave"), "")
                --TODO: Load game
            else
                window = "create"
                currentmenu = saves
            end
            UI.Alert.change(overwrite, 2)                              --Reset default selection
        else                                                    --Otherwise, select menu item and load animation
            index = UI.Menu.getIndex(currentmenu)
            enableCount = true
        end
    end
    if buttons.released.circle and buttonEnabled.circle then    --Hide alerts if shown or go back to main menu
        if showerror then                                           --Alerts
            showerror = false
        elseif showdelete then
            showdelete = false
            UI.Alert.change(delete, 2)
        elseif window == "load" then                                --Menus
            window = "main"
            currentmenu = main
        elseif window == "create" then
            window = "main"
            currentmenu = main
        elseif window == "settings" then
            window = "main"
            currentmenu = main
        elseif window == "overwrite" then
            window = "create"
            UI.Alert.change(overwrite, 2)
            currentmenu = saves
        end
        index = nil
    end
    if buttons.released.triangle and buttonEnabled.triangle then --Deletion
        if UI.Menu.getIndex(currentmenu) == 1 then                  --Check the current selection
            if checksave(1) then                                    --Check if file exists. If it does, show delete alert, otherwise, show error
                showdelete = true
                currentalert = delete
                settings.set("lastsave", 1)
            else
                showerror = true
            end
        elseif UI.Menu.getIndex(currentmenu) == 2 then
            if checksave(2) then
                showdelete = true
                currentalert = delete
                settings.set("lastsave", 2)
            else
                showerror = true
            end
        elseif UI.Menu.getIndex(currentmenu) == 3 then
            if checksave(3) then
                showdelete = true
                currentalert = delete
                settings.set("lastsave", 3)
            else
                showerror = true
            end
        elseif UI.Menu.getIndex(currentmenu) == 4 then
            if checksave(4) then
                showdelete = true
                currentalert = delete
                settings.set("lastsave", 4)
            else
                showerror = true
            end
        end
        index = nil
    end

    if enableCount then                                         --Menu selection animation
        count = count + 1
        if count == 4 then                                          --Count up to 0.016*4ms and do actions depending
            count = 0                                               --on current menu and selection
            enableCount = false
            if window == "main" then                                --Main menu
                if index == 1 then
                    window = "load"
                    currentmenu = saves
                elseif index == 2 then
                    window = "create"
                    currentmenu = saves
                elseif index == 3 then
                    window = "settings"
                    currentmenu = config
                elseif index == 4 then
                    os.exit()
                end
            elseif window == "load" then                            --Load menu
                if index == 1 then
                    if not checksave(1) then
                        showerror = true
                        currentalert = error
                    else
                        settings.set("lastsave", 1)
                    end
                elseif index == 2 then
                    if not checksave(2) then
                        showerror = true
                        currentalert = error
                    else
                        settings.set("lastsave", 2)
                    end
                elseif index == 3 then
                    if not checksave(3) then
                        showerror = true
                        currentalert = error
                    else
                        settings.set("lastsave", 3)
                    end
                elseif index == 4 then
                    if not checksave(4) then
                        showerror = true
                        currentalert = error
                    else
                        settings.set("lastsave", 4)
                    end
                end
                --TODO: Load game
            elseif window == "create" then                          --New game menu
                local lastsave
                if index == 1 then
                    lastsave = 1
                    settings.set("lastsave", 1)
                elseif index == 2 then
                    lastsave = 2
                    settings.set("lastsave", 2)
                elseif index == 3 then
                    lastsave = 3
                    settings.set("lastsave", 3)
                elseif index == 4 then
                    lastsave = 4
                    settings.set("lastsave", 4)
                end
                if checksave(lastsave) then
                    window = "overwrite"
                    currentalert = overwrite
                else
                    --TODO: Load game
                end
            elseif window == "settings" then                        --Settings menu
                if index == 1 then
                    if soundStatus then
                        sound.stop(background)
                        soundStatus = false
                    else
                        sound.play(background)
                        soundStatus = true
                    end
                    settings.set("sound", soundStatus)
                elseif index == 2 then
                    if autosaveStatus then
                        autosaveStatus = false
                    else
                        autosaveStatus = true
                    end
                    settings.set("autosave", autosaveStatus)
                elseif index == 3 then
                    if debugStatus then
                        debugStatus = false
                    else
                        debugStatus = true
                    end
                    settings.set("debug", debugStatus)
                elseif index == 4 then

                end
            end
            index = nil                                             --Reset index to hide clicking animation
        end
    end
end

function debug()
    if debugStatus then
        function round(num, numDecimalPlaces)
            local mult = 10^(numDecimalPlaces or 0)
            return math.floor(num * mult + 0.5) / mult
        end
        screen.print(5, 5, "FPS: " .. screen.fps() .. " RAM:" .. round((os.ram()/1024)/1024, 2) .. "MB" .. " - menuIndex: " .. UI.Menu.getIndex(currentmenu) .. " - count: " .. count, 0.5)
    end
end

-- START - Don't modify this block
setup()
while true do
    buttons.read()
    buttons.run()
    loop()
    debug()
    screen.flip()
    collectgarbage("collect")
end
-- END - Don't modify this block
