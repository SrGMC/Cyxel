colors = {color.new(63, 50, 174), color.new(227, 14, 194), color.new(186, 170, 255), color.new(255, 255, 255), color.new(255, 148, 157),
color.new(232, 2, 0), color.new(122, 36, 61), color.new(0, 0, 0), color.new(25, 86, 72), color.new(106, 137, 39), color.new(22, 237, 117),
color.new(50, 193, 195), color.new(5, 127, 193), color.new(110, 78, 35), color.new(201, 143, 76), color.new(239, 227, 5), color.new(214, 160, 144),
color.new(254, 59, 30), color.new(161, 44, 50), color.new(250, 47, 122), color.new(251, 159, 218), color.new(230, 28, 247), color.new(153, 47, 124),
color.new(71, 1, 31), color.new(5, 17, 85), color.new(79, 2, 236), color.new(45, 105, 203), color.new(0, 166, 238), color.new(111, 235, 255),
color.new(8, 162, 154), color.new(42, 102, 106), color.new(6, 54, 25), color.new(74, 73, 87), color.new(142, 123, 164), color.new(183, 192, 255),
color.new(172, 190, 156), color.new(130, 124, 112), color.new(90, 59, 28), color.new(174, 101, 7), color.new(247, 170, 48), color.new(244, 234, 92),
color.new(155, 149, 0), color.new(86, 98, 4), color.new(17, 150, 59), color.new(81, 225, 19), color.new(8, 253, 204), color.new(26, 188, 156),
color.new(22, 160, 133), color.new(241, 196, 15), color.new(243, 156, 18), color.new(46, 204, 113), color.new(39, 174, 96), color.new(230, 126, 34),
color.new(211, 84, 0), color.new(52, 152, 219), color.new(41, 128, 185), color.new(231, 76, 60), color.new(192, 57, 43), color.new(155, 89, 182),
color.new(142, 68, 173), color.new(236, 240, 241), color.new(189, 195, 199), color.new(52, 73, 94), color.new(44, 62, 80), color.new(149, 165, 166),
color.new(127, 140, 141), color.new(14,19,50)}

function colors.Sapphire()
    return colors[1]
end

function colors.HotMagenta()
    return colors[2]
end

function colors.PaleViolet()
    return colors[3]
end

function colors.White()
    return colors[4]
end

function colors.RosePink()
    return colors[5]
end

function colors.Red()
    return colors[6]
end

function colors.Wine()
    return colors[7]
end

function colors.Black()
    return colors[8]
end

function colors.DarkBlueGreen()
    return colors[9]
end

function colors.MossyGreen()
    return colors[10]
end

function colors.MintyGreen()
    return colors[11]
end

function colors.Topaz()
    return colors[12]
end

function colors.Cerulean()
    return colors[13]
end

function colors.MudBrown()
    return colors[14]
end

function colors.DullOrange()
    return colors[15]
end

function colors.PissYellow()
    return colors[16]
end

function colors.PinkishTan()
    return colors[17]
end

function colors.OrangeyRed()
    return colors[18]
end

function colors.Rouge()
    return colors[19]
end

function colors.StrongPink()
    return colors[20]
end

function colors.BubblegumPink()
    return colors[21]
end

function colors.Pink_Purple()
    return colors[22]
end

function colors.WarmPurple()
    return colors[23]
end

function colors.Burgundy()
    return colors[24]
end

function colors.NavyBlue()
    return colors[25]
end

function colors.Blue_Purple()
    return colors[26]
end

function colors.MediumBlue()
    return colors[27]
end

function colors.Azure()
    return colors[28]
end

function colors.RobinsEgg()
    return colors[29]
end

function colors.Blue_Green()
    return colors[30]
end

function colors.DarkAqua()
    return colors[31]
end

function colors.DarkForestGreen()
    return colors[32]
end

function colors.CharcoalGrey()
    return colors[33]
end

function colors.GreyishPurple()
    return colors[34]
end

function colors.LightPeriwinkle()
    return colors[35]
end

function colors.GreenishGrey()
    return colors[36]
end

function colors.MediumGrey()
    return colors[37]
end

function colors.Brown()
    return colors[38]
end

function colors.Umber()
    return colors[39]
end

function colors.YellowishOrange()
    return colors[40]
end

function colors.Yellowish()
    return colors[41]
end

function colors.PeaSoup()
    return colors[42]
end

function colors.MudGreen()
    return colors[43]
end

function colors.KelleyGreen()
    return colors[44]
end

function colors.ToxicGreen()
    return colors[45]
end

function colors.BrightTeal()
    return colors[46]
end

function colors.Turquoise()
    return colors[47]
end

function colors.GreenSea()
    return colors[48]
end

function colors.Sunflower()
    return colors[49]
end

function colors.Orange()
    return colors[50]
end

function colors.Emerald()
    return colors[51]
end

function colors.Nephritis()
    return colors[52]
end

function colors.Carrot()
    return colors[53]
end

function colors.Pumpkin()
    return colors[54]
end

function colors.PeterRiver()
    return colors[55]
end

function colors.BelizeHole()
    return colors[56]
end

function colors.Alizarin()
    return colors[57]
end

function colors.Pomegranate()
    return colors[58]
end

function colors.Amethyst()
    return colors[59]
end

function colors.Wisteria()
    return colors[60]
end

function colors.Clouds()
    return colors[61]
end

function colors.Silver()
    return colors[62]
end

function colors.WetAsphalt()
    return colors[63]
end

function colors.MidnightBlue()
    return colors[64]
end

function colors.Concrete()
    return colors[65]
end

function colors.Asbestos()
    return colors[66]
end

function colors.DarkBlue()
    return colors[67]
end

function colors.size()
    result = (#colors - 1) / 2
    return result
end

return colors
