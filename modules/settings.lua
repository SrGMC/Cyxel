settings = {}

local path = "config.cfg"

function settings.get(key)
    local found = false
    local result
    local file = io.open(path, "r")

    for line in file:lines() do
        if line:find(key) then
            found = true
            result = line:sub(key:len()+4)
            if result:find(tostring(true)) then result = true
            elseif result:find(tostring(false)) then result = false end
        end
    end
    if not found then
        return nil
    end

    file:close()
    return result
end

function settings.set(key, value)
    lines = {}
    index = 1
    found = false
    settings = ""

    local file = io.open(path, "r")
    for line in file:lines() do
        lines[#lines +1] = line
    end
    for i = 1, #lines do
        if lines[i]:find(key) then
            found = true
            index = i
        end
    end
    file:close()

    if not found then
        index = #lines +1
    end

    file = io.open(path, "w")
    lines[index] = key .. " = " .. tostring(value)
    file:write(table.concat(lines, "\n"))
    file:close()
end

function settings.delete(key)
    lines = {}
    index = 1
    found = false
    settings = ""

    local file = io.open(path, "r")
    for line in file:lines() do
        lines[#lines +1] = line
    end
    for i = 1, #lines do
        if lines[i]:find(key) then
            found = true
            index = i
        end
    end
    file:close()

    if found then
        table.remove(lines, index)
    end

    file = io.open(path, "w")
    file:write(table.concat(lines, "\n"))
    file:close()
end

function settings.save(saveindex, data)
    local savepath = "saves/save" .. saveindex .. ".sav"

end

function settings.load(saveindex)
    local savepath = "saves/save" .. saveindex .. ".sav"

end

function settings.remove(saveindex)
    local savepath = "saves/save" .. saveindex .. ".sav"
    os.remove(savepath)
end

return settings
