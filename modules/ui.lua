local colors = require("modules.colors")

local UI = {}

--Menu
local Menu = {}
Menu.__index = Menu

function Menu.new(title, menulist, selected, unselected)
    local self = setmetatable({}, Menu)

    if (type(menulist) ~= "table") or (#menulist ~= 4)then
        error("Menu list should be a table with size 4")
    end
    if (type(title) ~= "string") then
        error("Title must be a string")
    end

    self.title = title
    self.menulist = menulist
    self.colors = {selected, unselected, unselected, unselected}
    self.emphasis = {selected, unselected}
    self.index = 1
    return self
end

--Index is used to show the clicked index
function Menu.show(self, index)
    screen.clear(colors.Black())

    local width = 480
    local height = 272

    local textWidth = screen.textwidth(self.title, 1.2)
    local textHeight = screen.textheight(1.2)

    screen.print((width-textWidth) / 2, 40, self.title, 1.2)
    draw.line((width-textWidth) / 2, 40 + textHeight, ((width-textWidth) / 2) + textWidth, 40 + textHeight, colors.White())

    if index == 1 then
        draw.fillrect (80, 50+33, width-80-80, textHeight+10, self.colors[1])
        screen.print(80 + 5, 50+41, self.menulist[1])
    else
        draw.fillrect (80, 50+33, width-80-80, textHeight+10, colors.MidnightBlue())
        draw.fillrect (80, 50+30, width-80-80, textHeight+10, self.colors[1])
        screen.print(80 + 5, 50+38, self.menulist[1])
    end

    if index == 2 then
        draw.fillrect (80, 50+73, width-80-80, textHeight+10, self.colors[2])
        screen.print(80 + 5, 50+81, self.menulist[2])
    else
        draw.fillrect (80, 50+73, width-80-80, textHeight+10, colors.MidnightBlue())
        draw.fillrect (80, 50+70, width-80-80, textHeight+10, self.colors[2])
        screen.print(80 + 5, 50+78, self.menulist[2])
    end

    if index == 3 then
        draw.fillrect (80, 50+113, width-80-80, textHeight+10, self.colors[3])
        screen.print(80 + 5, 50+121, self.menulist[3])
    else
        draw.fillrect (80, 50+113, width-80-80, textHeight+10, colors.MidnightBlue())
        draw.fillrect (80, 50+110, width-80-80, textHeight+10, self.colors[3])
        screen.print(80 + 5, 50+118, self.menulist[3])
    end

    if index == 4 then
        draw.fillrect (80, 50+153, width-80-80, textHeight+10, self.colors[4])
        screen.print(80 + 5, 50+161, self.menulist[4])
    else
        draw.fillrect (80, 50+153, width-80-80, textHeight+10, colors.MidnightBlue())
        draw.fillrect (80, 50+150, width-80-80, textHeight+10, self.colors[4])
        screen.print(80 + 5, 50+158, self.menulist[4])
    end
end

function Menu.change(self, index)
    self.colors[self.index] = self.emphasis[2]
    self.index = index
    self.colors[self.index] = self.emphasis[1]
end

function Menu.getIndex(self)
    return self.index
end

function Menu.setMenulist(self, menulist)
    if (type(menulist) ~= "table") or (#menulist ~= 4)then
        error("Menu list should be a table with size 4")
    end
    self.menulist = menulist
end

function Menu.setTitle(self, title)
    self.title = title
end

--Alert
local Alert = {}
Alert.__index = Alert

function Alert.new(title, body, option1, option2, titlecolor, bodycolor, background, selected, unselected, defaultindex)
    local self = setmetatable({}, Alert)

    if (type(title) ~= "string") then
        error("Title must be a string")
    end
    if (type(body) ~= "string") then
        error("Body must be a string")
    end
    if(not (defaultindex == 1 or defaultindex == 2)) then
        error("defaultindex must be a value between 1 and 2")
    end

    self.title = title
    self.body = body
    self.options = {option1, option2}
    self.colors = {titlecolor, bodycolor}
    self.background = background
    if defaultindex == 1 then
        self.optioncolors = {selected, unselected}
    else
        self.optioncolors = {unselected, selected}
    end
    self.emphasis = {selected, unselected}
    self.index = defaultindex
    return self
end

function Alert.show(self)
    screen.clear(colors.Black())

    local width = 480
    local height = 272

    local textWidth = screen.textwidth(self.title, 1.2)
    local textHeight = screen.textheight(1.2)
    local optwidth = screen.textwidth(self.options[1]) + 20 + screen.textwidth(self.options[2])
    local bodyheight = 17
    for i in string.gmatch(self.body, "\n") do bodyheight = bodyheight + 17 end
    bodyheight = bodyheight + 5

    screen.print((width-textWidth) / 2, 40, self.title, 1.2, self.colors[1])
    draw.line((width-textWidth) / 2, 40 + textHeight, ((width-textWidth) / 2) + textWidth, 40 + textHeight, self.colors[1])

    draw.fillrect(80, 50+33, width-80-80, bodyheight, self.background)
    screen.print(80 + 5, 50+38, self.body, 0.7, self.colors[2])

    screen.print((width-optwidth) / 2, 50+33+bodyheight+15, self.options[1], 0.7, self.optioncolors[1])
    screen.print(((width-optwidth) / 2) + screen.textwidth(self.options[1]) + 20, 50+33+bodyheight+15, self.options[2], 0.7, self.optioncolors[2])
    if(self.index == 1) then
        draw.line((width-optwidth) / 2, 50+33+bodyheight+30, ((width-optwidth) / 2) + screen.textwidth(self.options[1]) + 2, 50+33+bodyheight+30, self.emphasis[1])
    else
        draw.line(((width-optwidth) / 2) + screen.textwidth(self.options[1]) + 20, 50+33+bodyheight+30, ((width-optwidth) / 2) + screen.textwidth(self.options[1]) + 20 + screen.textwidth(self.options[2]) + 1, 50+33+bodyheight+30, self.emphasis[1])
    end
end

function Alert.change(self, index)
    self.optioncolors[self.index] = self.emphasis[2]
    self.index = index
    self.optioncolors[self.index] = self.emphasis[1]
end

function Alert.getIndex(self)
    return self.index
end

--Menubar
local Menubar = {}
Menubar.__index = Menubar

function Menubar.new(items, itemimage, backgroundcolor, highlightcolor, text, textcolor)
    local self = setmetatable({}, Menubar)
    local width = 480
    local height = 272

    if not (items >= 1 and items <= 20) then
        error("items should be a number between 1 and 20")
    end
    if screen.textwidth(text, 0.5) > width-(10+(17*items)) then
        error("text is bigger than " .. width-(10+(17*items)) .. "px. Try shortening it or reducing the number of items")
    end

    self.items = items
    self.image = image.load(itemimage)
    self.colorlist = {}
    for i=1, items do
        self.colorlist[i] = backgroundcolor
    end
    self.colorlist[1] = highlightcolor
    self.colors = {backgroundcolor, highlightcolor, textcolor}
    self.text = text
    self.index = 1
    return self
end

function Menubar.show(self)
    local x = 0
    local count = 1
    repeat
        draw.fillrect(x, 0, 16, 16, self.colorlist[count])
        x = x+17
        count = count +1
    until (count == self.items+1)
    draw.fillrect((17*self.items), 0, width-(17*self.items), 16, self.colors[1])
    image.blit(self.image, 0, 0)
    screen.print((17*self.items)+5, 0, self.text, 0.5, self.colors[3])
end

function Menubar.getIndex(self)
    return self.index
end

function Menubar.change(self, index)
    self.colorlist[self.index] = self.colors[1]
    self.index = index
    self.colorlist[self.index] = self.colors[2]
end

function Menubar.setText(self, text)
    if screen.textwidth(text, 0.5) > width-(10+(17*self.items)) then
        error("text is bigger than " .. width-(10+(17*self.items)) .. "px. Try shortening it or reducing the number of items")
    end
    self.text = text
end

function Menubar.maxItems(self)
    return self.items
end

UI.Menu = Menu
UI.Alert = Alert
UI.Menubar = Menubar

return UI
